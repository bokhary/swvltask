//
//  NetworkProvider_Tests.swift
//  SwvlTaskTests
//
//  Created by Elsayed Hussein on 5/6/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import XCTest
@testable import SwvlTask

class NetworkProvider_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    //MARK: NetworkManger Asynchronous Testing
    func testRequestMethodAsynchronous() {
        
        var message: String?
        var serviceData: Data?
        // to handle asynchronous call
        let expect = expectation(description: "Data received")
        NetworkManager.request(serviceName: WebServices.linesService(), method: .get) { (result) in
            switch result {
            case .fail(let failMessage):
                message = failMessage
            case .success(let data):
                serviceData = data
            }
            // end expectation
            expect.fulfill()
        }
        // timout
        waitForExpectations(timeout: 5, handler: nil)
        
        //        test
        XCTAssertNil(message)
        XCTAssertNotNil(serviceData)
    }
    
    /**
     Test empty service name
     */
    func testRequestMethodWithEmptyServiceName() {
        NetworkManager.request(serviceName: "", method: .get, completion: { (result) in
            switch result {
            case .fail(let message):
                XCTAssertEqual(message ,"Empty service name")
            case .success(_):
                break
            }
        })
    }
    
    /**
     Test serivce if it has space in its name
     */
    func testRequestMethodWithServiceNameHasSpace() {
        NetworkManager.request(serviceName: Constants.baseURL + " ", method: .get, completion: { (result) in
            switch result {
            case .fail(let message):
                XCTAssertEqual(message ,"Service name contains space")
            case .success(_):
                break
            }
        })
    }

}
