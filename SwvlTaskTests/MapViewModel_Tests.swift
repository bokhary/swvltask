//
//  MapViewModel.swift
//  SwvlTaskTests
//
//  Created by Elsayed Hussein on 5/6/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import XCTest
@testable import SwvlTask


class MapViewModel_Tests: XCTestCase {
    
    /**
     Test numberOfRows method
     */
    func testNumberOfRowsMethod() {
        let mapViewModel = MapViewModel()
        // lineRoot nil
        let result = mapViewModel.numberOfRows()
        XCTAssertNil(mapViewModel.lineRoot)
        XCTAssertEqual(result, 0)
    }
    
    /**
     Test getLineOf Index method
     */
    func testgetLineOfIndexMethod() {
        
        let mapViewModel = MapViewModel()
        mapViewModel.lineRoot = LineRoot.init(lines: [Line(name: "ddd", stations: [])])
        let line2 = mapViewModel.getLineOf(index: 0)
        XCTAssertNotNil(line2)
        
        let line = mapViewModel.getLineOf(index: -1)
        XCTAssertNil(line)
        
        let line3 = mapViewModel.getLineOf(index: 5)
        XCTAssertNil(line3)
    }
    
    /**
     Test getLineOf Index method
     */
    func testgetStattionsLineOfIndexMethod() {
        let mapViewModel = MapViewModel()
        let line = mapViewModel.getLineOf(index: 0)
        XCTAssertNil(line)
        
        mapViewModel.lineRoot = LineRoot.init(lines: [Line(name: "ddd", stations: [])])
        let line2 = mapViewModel.getLineOf(index: 0)
        XCTAssertNotNil(line2)
        
        let line3 = mapViewModel.getLineOf(index: -1)
        XCTAssertNil(line3)
    }
    
}
