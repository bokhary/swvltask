//
//  HelperMethods_Tests.swift
//  SwvlTaskTests
//
//  Created by Elsayed Hussein on 5/6/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import XCTest
@testable import SwvlTask

class HelperMethods_Tests: XCTestCase {
    
    /**
     Test setShadow method
     */
    func testSetShadowMethod() {
        let result = HelperMethods.setShadow(view: UIView(), roundDegree: -1)
        XCTAssertEqual(result, .fail)
        
        let result2 = HelperMethods.setShadow(view: UIView(), roundDegree: 3)
        XCTAssertEqual(result2, .success)

    }
    
    /**
     Test alert method
     */
    func testAlertMethod() {
        let result = HelperMethods.alert(title: "Hello", message: "", viewController: UIViewController())
        XCTAssertEqual(result, .fail)
        
        let result2 = HelperMethods.alert(title: "", message: "Welcome Swvl", viewController: UIViewController())
        XCTAssertEqual(result2, .success)
    }
}
