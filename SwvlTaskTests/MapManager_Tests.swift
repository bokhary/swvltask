//
//  MapManager_Tests.swift
//  SwvlTaskTests
//
//  Created by Elsayed Hussein on 5/6/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import XCTest
@testable import SwvlTask
@testable import GoogleMaps

class MapManager_Tests: XCTestCase {
    
    var mapManager: MapManager!
    override func setUp() {
        super.setUp()
        mapManager = MapManager.init(mapView: GMSMapView())
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /**
     Test moveToCurrentLocation method
     */
    func testMoveToCurrentLocation() {
        // test with nil location
        let result = mapManager.moveToCurrentLocation()
        XCTAssertEqual(result, .fail)
        
        // initalize location
        mapManager.currentLocation = CLLocation.init()
        let result2 = mapManager.moveToCurrentLocation()

        XCTAssertNotEqual(result2, .fail)
    }
    
    /**
     Test plotsStationsOf Stations method
     */
    func testplotsStationsOfLocations() {
        // test with nil location
        let result = mapManager.plotsStationsOf(stations: [])
        XCTAssertNotEqual(result, .success)
        
        // initalize location
        let stations = [Station(id: 1, name: "Station1", image: "image1", address: "Address1", location: Location.init(latitude: 23, longitude: 23)), Station(id: 2, name: "Station2", image: "image2", address: "Address2", location: Location.init(latitude: 23, longitude: 23))]
        let result2 = mapManager.plotsStationsOf(stations: stations)
        
        XCTAssertEqual(result2, .success)
    }
    
    /**
     Test drawPolylineOf path method
     */
    func testdrawPolylineOfPath() {
        // test with nil location
        let path = GMSMutablePath.init()
        let result = mapManager.drawPolylineOf(path: path)
        XCTAssertEqual(result, .fail)
        
        // initalize location
        let path2 = GMSMutablePath.init()
        path2.add(CLLocationCoordinate2D.init(latitude: 22, longitude: 33))
        path2.add(CLLocationCoordinate2D.init(latitude: 44, longitude: 43))
        let result2 = mapManager.drawPolylineOf(path: path2)
        
        XCTAssertEqual(result2, .success)
    }
    
}
