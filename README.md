# SwvlTask

Swvl Task plots stations on map and draw routes between them.

## Getting Started


## The structre of project is :

### Moduls groupe: has core of project, it canotains:
### Map group: contains
* ### Views group: contains storyboard of Map view and Station view.
* ### Controllers group: contains MapViewController, MapViewModel, StationViewController, and StationViewModel
* ### Map Helpers group: contains classes that help map controller to draw and animate the markers and lines.
### Custom cells group: contains custom cell of lines collection view .
### Custom views group: contains custom views of InfoViewWindow of markers .

### Helpers group:  contains helper classes for project that make more easier .
### Models group:  contains model for lines and bookmark response . 
### Network group: contains classes helps in building network calling .

## Architecture
* ### I used MVVM design pattern to make controller has less massive code and also to avoid controller from business logic side, and make that responsiplity for ViewModel,  also to make project more testable.  also  ViewModel holde all work of fetching and parsing data from network.


## Implementation
* ### Firstly I fetched lines from service then I used collecion view to show lines. and when user click on line map manger plots the animated stations on map, then draw lines between them with animation, and finally move the bus on the lines.
* ### I used timer to make all animation of drawing marker and routes on map view
* ### I observed that the lines not drawn smoothly and also the bus movement, so i used Google Direction API to get the route between the start and end stations so you will see Segemented Controle where you can select to draw routes with simple route or Google Direction API route.
* ### I used closure to bind ViewModel to its controller and also to listen for call back like tap marker in MapManager class.
* ### I used custom views to handle InfoViewWindow of markers and cell.
* ### I used location manager to get current location.



### Prerequisites

I didn't commit libraries because of huge size, so please run pod install to get required librararies,.
