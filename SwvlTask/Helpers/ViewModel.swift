//
//  ViewModel.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/4/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

/**
 ViewModel is parent class for view models,
 hold common cloure used in all child view models.
 */
class ViewModel: NSObject {
    
    //MARK: Varaibles
    var sucess: () -> Void = {} // binding view model with its controller by updateView closure
    var showFailMessage: (String) -> Void = {_ in} // for fail response
    var loadingDone: () -> Void = {} // to handle activity indicator and blure view
}
