//
//  MethodResponse.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/6/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation
/**
 MethodResponse is enum to show fail and success method response
 */
enum MethodResponse {
    case fail
    case success
}
