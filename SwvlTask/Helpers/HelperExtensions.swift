//
//  HelperExtensions.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/5/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation


extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
