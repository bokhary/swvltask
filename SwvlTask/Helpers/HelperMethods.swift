//
//  HelperMethods.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/3/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import GoogleMaps

/**
 HelperMethods class has methods that we use alawys in our apps
 */
class HelperMethods {
    
    //MARK: Methods
    
    /**
     Set shadow effect on view
     - parameters:
     - view : the view to set shadow name want to request
     - roundDegree: degree of round for view
     - color: color of shadow, default is black
     */
    @discardableResult class func setShadow(view: UIView, roundDegree: CGFloat, color: UIColor = UIColor.black) -> MethodResponse {
        
        // set shadow
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity  = 0.3
        view.layer.shadowRadius = 2
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        
        view.layer.masksToBounds = false
        
        // round view corner
        if roundDegree < 0 {
            return .fail
        }
        view.layer.cornerRadius = roundDegree
        return .success
    }
    
    /**
     Alert message for user
     - parameters:
     - title : alert title
     - message: alert message
     - viewController: viewController that will show the alert
     */
    @discardableResult class func alert(title: String,message :String,viewController: UIViewController) -> MethodResponse{
        
        if !message.isEmpty {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(okAction)
            viewController.present(alert, animated: true, completion: nil)
            return .success
        }
        return .fail
    }
    
    /**
     Helper method to handle rotate of bus from old postion to new position
     - parameters:
     - fromCoordinate: old position of marker
     - toCoordinate: new position
     */
    
    class func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    
}
