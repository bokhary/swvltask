//
//  MapViewModel.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/3/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit

/**
 MapViewModel for MapViewController to handle
 business logic for controller, it extend ViewModel that has common clousre noeeded for all view models
 */
class MapViewModel: ViewModel {
    
    //MARK: Variables

    var lineRoot: LineRoot! // root of lines response
    
    //MARK: Methods
    
    /**
     Fetch lines data
     */
    func fetchLines() {
        NetworkManager.request(serviceName: WebServices.linesService(), method: .get) { (result) in
            // stop loading
            self.loadingDone()
            
            switch result {
            case .success(let data):
                self.parseData(data: data)
                self.sucess()
            case .fail(let message):
                self.showFailMessage(message)
            }
        }
    }
    
    /**
     Parse services response data
     - parameters:
     - data : the data to parse
     */
    private func parseData(data: Data) {
        do {
            let decoder = JSONDecoder()
            lineRoot = try decoder.decode(LineRoot.self, from: data)
        }
        catch {
            print("Errot \(error.localizedDescription)")
        }
    }
    
    /**
     Number of Lines collection view sections
     - Returns: return number of sections
     */
    func numberOfSections() -> Int {
        return 1
    }
    
    /**
     Number of Lines collection view rows
     - Returns: return number of rows
     */
    func numberOfRows() -> Int {
        guard let lineRoot = lineRoot else {
            return 0
        }
        return lineRoot.lines.count
    }
    
    /**
     Get specific line by index
     - parameters:
     - index : index of the line to get
     - Returns: return line
     */
    func getLineOf(index: Int) -> Line! {
        guard let lineRoot = lineRoot else {
            return nil
        }
        
        if index < 0 {
            return nil
        }
        
        if index < lineRoot.lines.count {
            return lineRoot.lines[index]
        }
        return nil
    }
    
    /**
     Get specific line stations  by index
     - parameters:
     - index : index of the line to get its stations array
     - Returns: return array of line stations
     */
    func getStattionsLineOf(index: Int) -> [Station]! {
        guard let lineRoot = lineRoot else {
            return nil
        }
        
        if index < 0 {
            return nil
        }
        
        if index < lineRoot.lines.count {
            return lineRoot.lines[index].stations
        }
        return nil
    }
    
    
}
