//
//  MapViewController.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/3/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import GoogleMaps

/**
 MapViewController is view controller to handle lines and show its staions on map, also you can see the bus moving on map.
 */
class MapViewController: UIViewController {
    
    //MARK: Variables
    var mapManger: MapManager!
    var loadingTimer: Timer!
    var loadingProgressValue: CGFloat = 0
    
    //MARK: View lifcycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup views
        setupViews()
        
        
        
        // fetch lines data
        startLoadingProgress()
        viewModel.fetchLines()
        
        // listen to response of lines data fetching
        bindSuccessClosure()
        
        // listen to loading closure to stop activity indicator
        bindLoadingDoneClosur()
        
        // listen to fail response
        bindFailMessageClosure()
        
        // listen to marker tapped
        handleMarkerTapped()
    }
    
    //MARK: Outlets
    @IBOutlet var viewModel: MapViewModel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var linesCollectionView: UICollectionView!
    @IBOutlet weak var blureVisualEffectiew: UIVisualEffectView!
    @IBOutlet weak var bottomLineCllectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var loadingCircularProgress: RPCircularProgress!
    
    @IBOutlet weak var drawRouteSegmentedControl: UISegmentedControl!
    
    
    //MARK: Actions
    @IBAction func directionRouteSegementedAction(_ sender: UISegmentedControl) {
        mapManger.drawType = sender.selectedSegmentIndex == 0 ? .simple : .directionAPI
        guard let selectedIndexPath = linesCollectionView.indexPathsForSelectedItems, let indexPath = selectedIndexPath.first else {
            return
        }
        let stations = viewModel.getStattionsLineOf(index: indexPath.row)
        if stations != nil {
            mapManger.plotsStationsOf(stations: stations!)
        }
    }
    
    @IBAction func currentLocationButtonAction(_ sender: UIButton) {
        mapManger.moveToCurrentLocation()
    }
    //MARK: Methods
    func setupViews() {
        
        // register line cell to collection
        let lineCellNib = UINib.init(nibName: LineCollectionViewCell.nibName, bundle: nil)
        linesCollectionView.register(lineCellNib, forCellWithReuseIdentifier: LineCollectionViewCell.identifier)
        
        // init map manager
        mapManger = MapManager.init(mapView: mapView)
        
    }
    
    /**
     Run loading progress view
     */
    func startLoadingProgress() {
        loadingTimer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (timer) in
            self.loadingCircularProgress.updateProgress(self.loadingProgressValue)
            self.loadingProgressValue += 0.01
            if self.loadingProgressValue > 1.2 {
                self.loadingProgressValue = 0
                self.loadingCircularProgress.updateProgress(0)
            }
        }
    }
    
    /**
     Stop progress loading
     */
    func stopProgressLoading() {
        if loadingTimer != nil {
            loadingTimer.invalidate()
            loadingCircularProgress.isHidden = true
        }
    }
    
    /**
     listen to success closure when data of lines come.
     */
    func bindSuccessClosure() {
        viewModel.sucess = { [unowned self] in // capture list to break strong retain cycle
            
            DispatchQueue.main.async { // to update view call it in main thread
                self.linesCollectionView.reloadData()
                
                UIView.animate(withDuration: 0.5,
                               delay: 0,
                               usingSpringWithDamping: 0.8,
                               initialSpringVelocity: 1,
                               options: UIViewAnimationOptions.beginFromCurrentState,
                               animations: {
                                self.drawRouteSegmentedControl.isHidden = false
                                self.bottomLineCllectionConstraint.constant = -20
                                self.view.layoutIfNeeded()
                                
                }, completion: nil)
                UIView.animate(withDuration: 0.5, animations: {
                    
                })
            }
        }
    }
    
    /**
     listen to loadind done closure
     */
    func bindLoadingDoneClosur() {
        viewModel.loadingDone = { [unowned self] in // capture list to break strong retain cycle
            self.stopProgressLoading()
            self.blureVisualEffectiew.isHidden = true
        }
    }
    
    /**
     listen to fail network message
     */
    func bindFailMessageClosure() {
        viewModel.showFailMessage = { [unowned self] message in // capture list to break strong retain cycle
            HelperMethods.alert(title: "Network Message", message: message, viewController: self)
        }
    }
    
    /**
     Present Station Details screen with appropraite station when marker tapped
     */
    func handleMarkerTapped() {
        mapManger.markerTapped = { [unowned self] station in
            self.performSegue(withIdentifier: "PresentStationDetails", sender: station)
        }
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PresentStationDetails" {
            let stationDetailViewController = segue.destination as! StationDetailsViewController
            stationDetailViewController.station = sender as! Station
        }
    }
    
    
}
extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    //MARK: UICollectionViewDataSource methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LineCollectionViewCell.identifier, for: indexPath) as! LineCollectionViewCell
        let line = viewModel.getLineOf(index: indexPath.row)
        if line != nil {
            cell.configure(line: line!)
        }
        return cell
    }
    
    
    //MARK: UICollectionViewDelegate method
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // draw marker and line of stations on map
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        let stations = viewModel.getStattionsLineOf(index: indexPath.row)
        if stations != nil {
            mapManger.plotsStationsOf(stations: stations!)
        }
    }
}
