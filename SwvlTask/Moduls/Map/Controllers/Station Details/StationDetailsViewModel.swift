//
//  StationDetailsViewModel.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/4/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

/**
 MapViewModel for StationDetailsViewController to handle
 business logic for controller, it extend ViewModel that has common clousre noeeded for all view models
 */
class StationDetailsViewModel: ViewModel {
    
    //MARK: Variables
    var bookmarkResponse: Bookmark!
    
    //MARK: Methods
    
    /**
     Call bookmark service to bookmark station
     - Parameters
     - id : station id
     */
    func bookmarkStationOf(id: Int) {
        NetworkManager.request(serviceName: WebServices.bookmarkStationOf(id: id), method: .post) { (result) in
            
            // fire loading closuer
            self.loadingDone()
            
            switch result {
            case .success(let data):
                // parse data
                self.parseData(data: data)
                // fire success closure
                self.sucess()
            case .fail(let message):
                // fire fail closure
                self.showFailMessage(message)
            }
        }
    }
    
    /**
     Parse services response data
     - parameters:
     - data : the data to parse
     */
    private func parseData(data: Data) {
        do {
            let decoder = JSONDecoder()
            bookmarkResponse = try decoder.decode(Bookmark.self, from: data)
        }
        catch {
            print("Errot \(error.localizedDescription)")
        }
    }
    
    
}
