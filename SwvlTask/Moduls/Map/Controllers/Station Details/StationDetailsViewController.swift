//
//  StationDetailsViewController.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/4/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import Kingfisher

/**
 StationDetailsViewController display the station details also you can bookmark station.
 */
class StationDetailsViewController: UIViewController {
    
    //MARK: Variables
    var station: Station!
    
    //MARK: View lifcycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup views
        setupViews()
        
        // listen to response of lines data fetching
        bindSuccessClosure()
        
        // listen to loading closure to stop activity indicator
        bindLoadingDoneClosur()
        
        // listen to fail response
        bindFailMessageClosure()
    }
    
    
    //MARK: Outlets
    @IBOutlet var viewModel: StationDetailsViewModel!
    @IBOutlet weak var stationImageView: UIImageView!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var stationAdressLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bookMarkButton: UIButton!
    
    //MARK: Actions
    @IBAction func bookmarkButtonAction(_ sender: UIButton) {
        // bookmark station
        sender.isSelected = true
        activityIndicator.startAnimating()
        viewModel.bookmarkStationOf(id: station.id)
    }
    @IBAction func dismissAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Methods
    func setupViews() {
        
        // set station data
        let imageURL = URL.init(string: station.image)
        stationImageView.kf.setImage(with: imageURL!)
        stationNameLabel.text = station.name
        stationAdressLabel.text = station.address
     
        //
        HelperMethods.setShadow(view: bookMarkButton, roundDegree: 5)
    }
    
    /**
     listen to success closure when data of lines come.
     */
    func bindSuccessClosure() {
        viewModel.sucess = { [unowned self] in // capture list to break strong retain cycle
            
            DispatchQueue.main.async { // to update view call it in main thread
                HelperMethods.alert(title: "Succcess", message: self.viewModel.bookmarkResponse!.message, viewController: self)
            }
        }
    }
    
    /**
     listen to loadind done closure
     */
    func bindLoadingDoneClosur() {
        viewModel.loadingDone = { [unowned self] in // capture list to break strong retain cycle
            self.activityIndicator.stopAnimating()
            self.bookMarkButton.isSelected = false
        }
    }
    
    /**
     listen to fail network message
     */
    func bindFailMessageClosure() {
        viewModel.showFailMessage = { [unowned self] message in // capture list to break strong retain cycle
            HelperMethods.alert(title: "Network Message", message: message, viewController: self)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
