//
//  MapAnimationWithDirectionAPI.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/5/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import GoogleMaps

/**
 MapAnimationWithDirectionAPI is helper class to draw  routes on map
 and also show bus moving using Google Direction Api to be more smoothly.
 */
class MapAnimationWithDirectionAPI: NSObject {
    
    //MARK: Varaibles
    var mapManager: MapManager!
    var timerCount = 0
    
    //MARK: Init methods
    init(mapManager: MapManager) {
        self.mapManager = mapManager
    }
    
    //MARK: Methods
    
    /**
     Draw lines between stations using Direction API
     - Paramters
     - stations: stations of lines
     */
    func drawLinesStations(stations: [Station])
    {
        getRouteBeteenStations(stations: stations) { (path) in
            guard let path = path else {
                return
            }
            self.drawLinesBetweenStations(path: path)
        }
    }
    
    /**
     Get route between stations by google direction api
     - parameters:
     - stations : stations of lines
     */
    func getRouteBeteenStations(stations: [Station], completion: @escaping (GMSPath?) -> ()) {
        let origin = "\(stations.first!.location.latitude),\(stations.first!.location.longitude)"
        let destination = "\(stations.last!.location.latitude),\(stations.last!.location.longitude)"
        
        
        NetworkManager.request(serviceName: WebServices.googleAPIDirectionService(origin: origin, destination: destination), method: .get) { (result) in
            switch result {
            case .success(let data):
                let path = self.getRoutePath(data: data)
                completion(path)
            case .fail(let message):
                print("Fail Message: \(message)")
            }
        }
    }
    
    /**
     Animate Bus using path of direction api
     */
    func animateBuse(path: GMSPath) {
        
        var oldCoodinate = path.coordinate(at: 0)
        
       mapManager.timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (timer) in
            if self.timerCount < path.count() {
                
                let newCoodinate = path.coordinate(at: UInt(self.timerCount))
                self.mapManager.busMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
                self.mapManager.busMarker.rotation = CLLocationDegrees(HelperMethods.getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
                
                self.mapManager.busMarker.position = oldCoodinate
                self.mapManager.busMarker.map = self.mapManager.mapView
                
                CATransaction.begin()
                CATransaction.setAnimationDuration(2)
                
                CATransaction.setCompletionBlock({() -> Void in
                    self.mapManager.busMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                    self.mapManager.busMarker.position = newCoodinate
                    self.mapManager.busMarker.map = self.mapManager.mapView
                    self.mapManager.busMarker.rotation = CLLocationDegrees(HelperMethods.getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
                    let cameraPostion = GMSCameraUpdate.setTarget(self.mapManager.busMarker.position, zoom: 15)
                    self.mapManager.mapView.animate(with: cameraPostion)
                    CATransaction.commit()
                    
                    oldCoodinate = newCoodinate
                    self.timerCount += 1
                })
            }
            else {
                self.timerCount = 0
                timer.invalidate()
            }
        }
    }
    
    /**
     Parse direction API response
     - Parameters
     - data: response of Direction API
     - Return: path between stations
     */
    private func getRoutePath(data: Data) -> GMSPath? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let routes = json["routes"] as! [AnyObject]
            
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"] as! [String:AnyObject]
                let points = routeOverviewPolyline["points"] as? String ?? ""
                let path = GMSPath.init(fromEncodedPath: points)
                return path
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        return nil
    }
    
    /**
     Draw lines between stations
     - parameters:
     - stations: stations of line to draw on map
     */
    private func drawLinesBetweenStations(path: GMSPath) {
        // crate mutable path to add locations
        let animatePath = GMSMutablePath.init()
        
        // draw lines
       mapManager.timer = Timer.scheduledTimer(withTimeInterval: 0.008, repeats: true, block: { (timer) in
            if self.timerCount < path.count() {                
                animatePath.add(path.coordinate(at: UInt(self.timerCount)))
                
                // draw line on map
                self.mapManager.drawPolylineOf(path: animatePath)
                
                // increament timer count
                self.timerCount += 1
                
            } else {
                
                // finally show bus on map
                self.mapManager.showBusMarkerOnMap()
                
                self.timerCount = 0
                timer.invalidate()
                self.animateBuse(path: path)
            }
        })
    }
}
