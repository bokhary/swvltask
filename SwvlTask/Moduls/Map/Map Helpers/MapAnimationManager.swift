//
//  MapAnimationManager.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/5/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import GoogleMaps

/**
 MapAnimationManger is helper class to draw staions and routes on map
 and also show bus moving .
 */
class MapAnimationManager: NSObject {
    
    //MARK: Varaibles
    var mapManager: MapManager!
    var timerCount = 0
    //MARK: Init methods
    init(mapManager: MapManager) {
        self.mapManager = mapManager
    }
    
    
    /**
     Animate Bus
     */
    func animateBuse(stations: [Station]) {

        var oldCoodinate = CLLocationCoordinate2D.init(latitude: stations[0].location.latitude, longitude: stations[0].location.longitude)

        mapManager.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            if self.timerCount < stations.count {
                let newCoodinate = CLLocationCoordinate2D.init(latitude: stations[self.timerCount].location.latitude, longitude: stations[self.timerCount].location.longitude)
                self.mapManager.busMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
                self.mapManager.busMarker.rotation = CLLocationDegrees(HelperMethods.getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
                self.mapManager.busMarker.position = oldCoodinate
                self.mapManager.busMarker.map = self.mapManager.mapView
                CATransaction.begin()
                CATransaction.setAnimationDuration(4)
                CATransaction.setCompletionBlock({() -> Void in
                    self.mapManager.busMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                    self.mapManager.busMarker.position = newCoodinate
                    self.mapManager.busMarker.map = self.mapManager.mapView
                    self.mapManager.busMarker.rotation = CLLocationDegrees(HelperMethods.getHeadingForDirection(fromCoordinate: oldCoodinate, toCoordinate: newCoodinate))
                    
                    // make bus marker appea in center of map view
                    let cameraUpdate = GMSCameraUpdate.setTarget(self.mapManager.busMarker.position, zoom: 15)
                    self.mapManager.mapView.animate(with: cameraUpdate)
                    //found bearing value by calculation
                    CATransaction.commit()
                    oldCoodinate = newCoodinate
                    self.timerCount += 1
                })
            }
            else {
                self.timerCount = 0
                timer.invalidate()
            }
        }
    }
    
    /**
     Plots sations on map using its location
     - parameters:
     - stations : stations of line  to draw on map
     */
    func showMarkersAndLines(stations: [Station]) {
        
        // plots stations marker on map
       mapManager.timer = Timer.scheduledTimer(withTimeInterval: 0.15, repeats: true, block: { (timer) in
            if self.timerCount < stations.count {
                let station = stations[self.timerCount]
                
                // the postion of station
                let coordinate = CLLocationCoordinate2D.init(latitude: station.location.latitude, longitude: station.location.longitude)
                
                // create marker
                let marker = self.mapManager.createMarkerOf(coordinate: coordinate, currentCoordinateIndex: self.timerCount, lastCoordinateIndex: stations.count - 1)
                
                // set station for marker to use it when show Info Window
                marker.userData = ["station": station]
                
                marker.appearAnimation = .pop
                marker.map = self.mapManager.mapView
                self.timerCount += 1
            } else {
                self.timerCount = 0
                timer.invalidate()
                // draw lines
                if self.mapManager.drawType == .simple {
                    self.drawLinesBetweenStations(stations: stations)
                }
                else {
                    self.mapManager.mapAnimationWithDirectionAPI.drawLinesStations(stations: stations)
                }
            }
        })
    }
    
    /**
     Draw lines between stations
     - parameters:
     - stations: stations of line to draw on map
     */
    private func drawLinesBetweenStations(stations: [Station]) {
        // crate mutable path to add locations
        let path = GMSMutablePath.init()
        
        // draw lines
       mapManager.timer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { (timer) in
            if self.timerCount < stations.count {
                // get ordered location
                let station = stations[self.timerCount]
                
                // the postion of station
                let coordinate = CLLocationCoordinate2D.init(latitude: station.location.latitude, longitude: station.location.longitude)
                
                path.add(coordinate)
                
                // draw line on map
                self.mapManager.drawPolylineOf(path: path)
                
                // increament timer count
                self.timerCount += 1
                
            } else {
                
                // finally show bus on map
                self.mapManager.showBusMarkerOnMap()
                
                self.timerCount = 0
                timer.invalidate()
                self.animateBuse(stations: stations)
            }
        })
    }
}
