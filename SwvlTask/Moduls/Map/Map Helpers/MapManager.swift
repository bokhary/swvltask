//
//  MapManager.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/4/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import GoogleMaps

/**
 MapManger is helper class to manage map view .
 */
class MapManager: NSObject {
    
    //MARK: Variables
    private var locationManager: CLLocationManager!
    private  var mapAnimationManager: MapAnimationManager!
    var currentLocation: CLLocation!
    var mapAnimationWithDirectionAPI: MapAnimationWithDirectionAPI!
    var drawType: DrawType = .simple
    var mapView: GMSMapView!
    var busMarker: GMSMarker!
    var markerTapped: (Station) -> Void = { _ in }
    var timer: Timer!
    
    enum DrawType {
        case simple
        case directionAPI
    }
    
    //MARK: Init methods
    init(mapView: GMSMapView) {
        super.init()
        
        self.mapView = mapView
        self.mapView.delegate = self
        
        // to show current location
        self.mapView.isMyLocationEnabled = true
        setupLocationManager()
        
        mapAnimationManager = MapAnimationManager.init(mapManager: self)
        mapAnimationWithDirectionAPI = MapAnimationWithDirectionAPI.init(mapManager: self)
    }
    
    //MARK: Methods
    
    /**
     Move map to current locatiom
     */
    @discardableResult func moveToCurrentLocation() -> MethodResponse {
        if currentLocation != nil {
            mapView.animate(toLocation: currentLocation.coordinate)
            locationManager.stopUpdatingLocation()
            return .success
        }
        return .fail
    }
    
    /**
     Plots sations on map using its location
     - parameters:
     - stations : stations of line  to draw on map
     */
    @discardableResult func plotsStationsOf(stations: [Station]) -> MethodResponse {
        
        if stations.count == 0 {
            return .fail
        }
        // prepare map view for drawing
        prepareMapView()
        
        // move camera to stations location
        adjustCameraLocationAndDrawRoutLine(stations: stations)
        
        // wait until adjut camera to location and show markers and lines on map animated
        timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false) { (timer) in
            self.mapAnimationManager.showMarkersAndLines(stations: stations)
            timer.invalidate()
        }
        return .success
    }
    
    /**
     Prepare map view to draw marker and lines
     */
    private func prepareMapView() {
        // clear map
        mapView.clear()
        
        // invalidate timer
        if timer != nil {
            self.mapAnimationManager.timerCount = 0
            self.mapAnimationWithDirectionAPI.timerCount = 0
            timer.invalidate()
        }
    }
    
    /**
     Setup location manager to handle current location of user
     */
    func setupLocationManager() {
        locationManager = CLLocationManager.init()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    /**
     Setup special marker of bus
     */
    func setupBusMarker(coordinate: CLLocationCoordinate2D) {
        busMarker = GMSMarker.init(position: coordinate)
        busMarker.icon = #imageLiteral(resourceName: "Bus")
        busMarker.infoWindowAnchor = CGPoint(x: 0.44, y: 0.45)
    }
    
    /**
     Show bus marker on map
     */
    func showBusMarkerOnMap() {
        busMarker.appearAnimation = .pop
        busMarker.map = mapView
        mapView.selectedMarker = busMarker
    }
    
    /**
     Draw polyline on map
     - parameters:
     - path: to draw polyline on map
     */
    @discardableResult func drawPolylineOf(path: GMSMutablePath) -> MethodResponse {
        if path.count() == 0 {
            return .fail
        }
        let polyLine = GMSPolyline.init(path: path)
        polyLine.strokeColor = #colorLiteral(red: 0.9960784314, green: 0.3725490196, blue: 0.3333333333, alpha: 1)
        polyLine.strokeWidth = 6
        polyLine.map = self.mapView
        return .success
    }
    
    /**
     Create markr with appropraite marker
     - parameters:
     - coordinate : marker position on map
     - currentCoordinateIndex: index of current coordinate to draw on map to and set correct marker icon
     - lastCoordinateIndex: index of last coordinate to draw on map and to set correct marker icon
     */
    func createMarkerOf(coordinate: CLLocationCoordinate2D, currentCoordinateIndex: Int, lastCoordinateIndex: Int) -> GMSMarker{
        
        let marker = GMSMarker.init(position: coordinate)
        
        // set correct icon for marker
        switch currentCoordinateIndex {
        case 0:
            marker.icon = #imageLiteral(resourceName: "StartPoint")
            // set bus marker on first location
            setupBusMarker(coordinate: coordinate)
        case lastCoordinateIndex:
            marker.icon = #imageLiteral(resourceName: "EndPoint")
        default:
            marker.icon = #imageLiteral(resourceName: "Point")
        }
        marker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        marker.infoWindowAnchor = CGPoint.init(x: 2.7, y: 0)
        return marker
    }
    
    /**
     Move map camera to see all stations location
     - parameters:
     - stations : the path that hold all coordinate of stations
     */
    func adjustCameraLocationAndDrawRoutLine(stations: [Station]) {
        
        // crate mutable path to add locations
        let path = GMSMutablePath.init()
        
        // iterate over  locations and get the path
        for index in 0..<stations.count {
            
            // get ordered location
            let station = stations[index]
            
            // the postion of station
            let coordinate = CLLocationCoordinate2D.init(latitude: station.location.latitude, longitude: station.location.longitude)
            
            // add coordinate to path
            path.add(coordinate)
        }
        
        // adjust camera location to line
        let bounds = GMSCoordinateBounds.init(path: path)
        
        let cameraUpdate = GMSCameraUpdate.fit(bounds)
        mapView.animate(with: cameraUpdate)
        
    }
    
}

extension MapManager: CLLocationManagerDelegate {
    //MARK: CLLocationManagerDelegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            currentLocation = location
            mapView.animate(toLocation: location.coordinate)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Update Error: \(error.localizedDescription)")
    }
}

extension MapManager: GMSMapViewDelegate {
    //MARK: GMSMapViewDelegate methods
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if marker == busMarker {
            let nibBusInfoWindow = UINib.init(nibName: "BusInfoWindowView", bundle: nil)
            return nibBusInfoWindow.instantiate(withOwner: self, options: nil).first! as? UIView
        } else {
            
            let nibDefaultInfoWindow = UINib.init(nibName: "DefaultInfoWindow", bundle: nil)
            let defaultInfoWindow =  nibDefaultInfoWindow.instantiate(withOwner: self, options: nil).first! as? DefaultInfoWindow
            
            if let userData = marker.userData as? [String: Any] {
                let station = userData["station"] as! Station
                defaultInfoWindow?.configure(station: station)
            }
            return defaultInfoWindow
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let userData = marker.userData as? [String: Any] {
            let station = userData["station"] as! Station
            markerTapped(station)
        }
    }
}
