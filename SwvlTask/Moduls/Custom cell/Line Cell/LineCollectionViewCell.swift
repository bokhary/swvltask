//
//  LineCollectionViewCell.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/3/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit

/**
 LineCollectionViewCell is custome cell for LinesCollectionView
 */
class LineCollectionViewCell: UICollectionViewCell {

    //MARK: Variables
    static let identifier = "LineCell"
    static let nibName = "LineCollectionViewCell"
    //MARK: init methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lineNameLabel: UILabel!
    @IBOutlet weak var lineDetailsLabel: UILabel!
    
    //MARK: Actions
    
    //MARK: Methods
    func configure(line: Line) {
        
        // set shadow for contianer view
        HelperMethods.setShadow(view: containerView, roundDegree: 4)
        
        lineNameLabel.text = line.name
        lineDetailsLabel.text = "Details \(line.name)"
    }
}
