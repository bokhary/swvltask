//
//  DefaultInfoWindow.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/4/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit

/**
 DefaultInfoWindow is InfoViewWindo for default marker 
 */
class DefaultInfoWindow: UIView {

    //MARK: init methods
    override func awakeFromNib() {
        configureStationImage()
    }
    
    //MARK: Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stationImageView: UIImageView!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var stationDetailsLabel: UILabel!
    
    //MARK: Actions
    
    //MARK: Methods
    func configure(station: Station) {
        HelperMethods.setShadow(view: containerView, roundDegree: 4)
        let imageURL = URL.init(string: station.image)
        
        stationImageView.kf.setImage(with: imageURL)
        
        stationNameLabel.text = station.name
        stationDetailsLabel.text = station.address
        
    }
    
    private func configureStationImage() {
        stationImageView.layer.cornerRadius = 5
        stationImageView.layer.masksToBounds = true
    }

}
