//
//  Bookmark.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/4/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

/**
Bookmark is model of response of bookmark service.
*/
struct Bookmark: Decodable {
    let message: String
}
