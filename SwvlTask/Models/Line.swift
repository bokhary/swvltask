//
//  Line.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/2/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

/**
 LineRoot is model of response of lines service
 */
struct LineRoot: Decodable {
    let lines: [Line]
}

struct Line: Decodable {
    let name: String
    var stations: [Station]
}

struct Station: Decodable {
    let id: Int
    let name: String
    let image: String
    let address: String
    let location: Location
    
    private enum CodingKeys: String, CodingKey {
        case id, name, address, location
        case image = "img_url"
    }
    
}

struct Location: Decodable {
    let latitude: Double
    let longitude: Double
}

