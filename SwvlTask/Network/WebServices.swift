//
//  WebServices.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/2/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

/**
 WebServices is class that contain all our services that
 we need to request
 */
class WebServices {
    
    /**
     Get service name of bus lines
     */
    class func linesService() -> String {
        return Constants.baseURL + "lines"
    }
    
    /**
     Get service name of bookmark station with specific id
     - Parameters
     - id: id of station
     */
    class func bookmarkStationOf(id: Int) -> String {
        return Constants.baseURL + "stations/\(id)/bookmark"
    }
    
    /**
     Get service path of google API direction
     - Parameters
     - origin: start position
     - destination: end position
     */
    class func googleAPIDirectionService(origin: String, destination: String) -> String {
        return "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.googleDirectionAPIKey)"
    }
    
}
