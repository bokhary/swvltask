//
//  NetworkResponse.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/3/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation
/**
 Result enum to handle response of network in its
 success and fails state
 */
enum Result {
    case success(Data)
    case fail(String)
}
