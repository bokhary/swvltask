//
//  NetworkProvider.swift
//  SwvlTask
//
//  Created by Elsayed Hussein on 5/2/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation
import Alamofire

/**
    NetworkManger is class handle service calling
 */
class NetworkManager {
    
    //MARK: Methods
    /**
     Request method wrap Alamofire request to make more simple
     - parameters:
     - serviceName : service name want to request
     - completion: cloasure to return data to controller
     */
    
    class func request(serviceName: String, method: HTTPMethod, completion: @escaping(Result) -> Void) {
        
        guard !serviceName.isEmpty else {
            completion(.fail("Empty service name"))
            return
        }
        guard !serviceName.contains(" ") else {
            completion(.fail("Service name contains space"))
            return
        }
        
        let url = URL.init(string: serviceName)!
        
        Alamofire.request(url, method: method).responseData { (response) in
            
            switch response.result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.fail(error.localizedDescription))
            }
        }
    }
    
    
}
